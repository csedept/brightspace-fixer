// ==UserScript==
// @name         Brightspace Fixer
// @namespace    http://tampermonkey.net/
// @version      2025-03-03+0
// @description  try to make brightspace a little more user friendly
// @author       jon denning, ben messick
// @match        https://taylor.brightspace.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=brightspace.com
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_setClipboard
// @homepageURL  https://repo.cse.taylor.edu/csedept/brightspace-fixer
// @updateURL    https://repo.cse.taylor.edu/csedept/brightspace-fixer/-/raw/main/tampermonkey.user.js
// @downloadURL  https://repo.cse.taylor.edu/csedept/brightspace-fixer/-/raw/main/tampermonkey.user.js
// ==/UserScript==

/*

This script is hosted at https://repo.cse.taylor.edu/csedept/brightspace-fixer
Install/update using: https://repo.cse.taylor.edu/csedept/brightspace-fixer/-/raw/main/tampermonkey.user.js

Tampermonkey docs: https://www.tampermonkey.net/documentation.php

*/

(function() {
    'use strict';

    const defaultOptions = {
        'full width': '98%',
        'panel width': 'min(30%, 500px)',
        'auto expand modules': 'false',
        'navbar sticky': 'true',
        'navbar email': 'true',
        'navbar grades': 'true',
        'navbar more': 'false',
        'navbar announcement': 'true',
        'navbar classlist': 'true',
        'navbar shorten': 'true',
        'date fixer': 'true',
        'title shorten': 'true',
        'courselist hide unpinned': 'true',
        'courselist shorten': 'true',
        'pagination': '100', // for some reason, 200 might not show up in dropdown
        'learner background': 'orange',
    }

    ///////////////////////////////////////////////////////////////
    // UTILITY FUNCTIONS
    // TODO: move to separate file and @require them?

    function isOverriding(k) {
        let v = GM_getValue(k)
        return v != null && v != undefined && v != defaultOptions[k]
    }
    function getOption(k) {
        return isOverriding(k) ? GM_getValue(k) : defaultOptions[k]
        //let v = GM_getValue(k)
        //if(v != null && v != undefined) return v
        //return defaultOptions[k]
    }
    function setOption(k, v) {
        GM_setValue(k, v)
    }
    // let sel = ['HTML', 'BODY.d2l-body.d2l-typography.vui-typography.daylight', 'D2L-CONSISTENT-EVALUATION#d2l_1_0_508.d2l-token-receiver', '#shadow-root', 'D2L-CONSISTENT-EVALUATION-PAGE', '#shadow-root', 'D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template', 'DIV', 'CONSISTENT-EVALUATION-RIGHT-PANEL', '#shadow-root', 'DIV.d2l-consistent-evaluation-right-panel', 'DIV.d2l-consistent-evaluation-right-panel-evaluation', 'CONSISTENT-EVALUATION-RIGHT-PANEL-EVALUATION', '#shadow-root', 'DIV', 'D2L-CONSISTENT-EVALUATION-RIGHT-PANEL-BLOCK', '#shadow-root', 'DIV.d2l-consistent-evaluation-right-panel-block', 'H3.d2l-label-text.d2l-skeletize']
    function arrayPush(array, elem) { array.push(elem) ; return array }
    function real(elem) { return elem ? (elem.shadowRoot || elem) : null }
    function query(selector, elem) {
        elem = real(elem) || document
        let selectors = selector.split(' #shadow-root > ')
        selectors.forEach((sel) => {
            if(!elem) return
            // if sel happens to end in #shadow-root, we can simply strip it (real does this already)
            sel.replaceAll('#shadow-root', '')
            elem = real(elem.querySelector(sel))
        })
        return elem
    }
    function concatString(pre, delim, post) {
        if(pre) return pre + delim + post
        return post
    }
    function createSelector(e) {
        if(!e || e == document || e == document.body) return ''
        if(e.host) { return createSelector(e.host) + ' #shadow-root' }
        let sel = e.tagName + (e.id ? ('#'+e.id) : '');
        if(e.classList) { e.classList.forEach((c) => { sel = sel + '.' + c }) }
        return concatString(createSelector(e.parentNode), ' > ', sel)
    }
    function callWhenExists(selector, fn_callback) {
        const maxAttempts = 100
        const totalTime = 10 * 1000

        let attempts = maxAttempts
        function fn() {
            if(attempts <= 0) return
            let elem = query(selector)
            if(elem) {
                attempts = 0
                fn_callback(elem)
            } else {
                attempts -= 1
                if(attempts > 0) setTimeout(fn, totalTime / maxAttempts)
            }
        }
        // attempt it now, but also try again after document finishes loading
        fn()
        document.body.addEventListener('load', fn)
    }
    function addElement(parentelem, tagname, attribs, prepend) {
        let elem = document.createElement(tagname.toUpperCase())
        for(let k in attribs) elem[k] = attribs[k]
        if(prepend) {
            parentelem.prepend(elem)
        } else {
            parentelem.append(elem)
        }
        return elem
    }
    function runningOnIFrame() {
        return window.location !== window.parent.location
    }
    function spacesToHyphens(k) { return k.replace(/ /g, '-') }
    function triggerMouseEvent(node, eventType) {
        let event = document.createEvent('MouseEvents')
        event.initEvent(eventType, true, true)
        node.dispatchEvent(event)
    }

    ///////////////////////////////////////////////////////////////
    // OPTIONS

    function resetDefaultOptions() {
        console.log('Reseting Brightspace Fixer Options')
        for(let k in defaultOptions) GM_setValue(k, undefined)
    }

    if(!runningOnIFrame()) {
        // add options dialog only to root document.body (not iframe)
        let optionsDialog = addElement(document.body, 'dialog', {
            className: 'bsp-fixer',
        })
        optionsDialog.style.top = "20px"
        let table = addElement(optionsDialog, 'table', {})
        let tbody = addElement(table, 'tbody', {})
        for(let k in defaultOptions) {
            let tr = addElement(tbody, 'tr', {})
            addElement(tr, 'td', { innerHTML: k })
            let td = addElement(tr, 'td', {})
            let input = addElement(td, 'input', {
                type: 'text',
                className: 'd2l-edit ' + (isOverriding(k) ? 'bsp-overriding' : ''),
                id: spacesToHyphens('bsp fixer ' + k),
            })
            input.addEventListener('change', ()=>{
                input.classList.add('bsp-overriding')
            })
        }

        let tdActions = addElement(addElement(tbody, 'tr', {}), 'td', { colspan:2 })

        let btnCancel = addElement(tdActions, 'button', {
            innerHTML:'CANCEL',
            title: 'Click to cancel any changes made to Brightspace Fixer options',
        })
        btnCancel.addEventListener('click', () => {
            let dialog = document.querySelector('dialog.bsp-fixer')
            dialog.close()
        })

        let btnSave = addElement(tdActions, 'button', {
            innerHTML:'SAVE',
            title: 'Click to save Brightspace Fixer options',
        })
        btnSave.addEventListener('click', () => {
            let dialog = document.querySelector('dialog.bsp-fixer')
            for(let k in defaultOptions) {
                let e = dialog.querySelector('input#' + spacesToHyphens('bsp fixer ' + k))
                setOption(k, e.value)
            }
            window.location.reload()
        })

        let btnReset = addElement(tdActions, 'button', {
            innerHTML:'RESET',
            title: 'Click to reset Brightspace Fixer options to defaults',
        })
        btnReset.addEventListener('click', () => {
            resetDefaultOptions()
            window.location.reload()
        })

        let btnSelector = addElement(tdActions, 'button', {
            innerHTML: 'SELECTOR',
            title: 'Click to get selector of hovered element (used for creating new Brightspace Fixer items)',
        })
        btnSelector.addEventListener('click', ()=>{
            query('dialog.bsp-fixer').close()
            startSelectorReporting()
        })

        let btnOptions = addElement(document.body, 'button', {
            innerHTML: '<span class="material-symbols-outlined">bolt</span>',
            className: 'bsp-fixer',
            title: 'Click to show Brightspace Fixer options',
        })
        btnOptions.addEventListener('click', () => {
            let dialog = document.querySelector('dialog.bsp-fixer')
            for(let k in defaultOptions) {
                let e = dialog.querySelector('input#' + spacesToHyphens('bsp fixer ' + k))
                e.value = getOption(k)
            }
            dialog.showModal()
        })

        addElement(document.head, 'style', {
            innerHTML: `
                input.bsp-overriding {
                    background-color: orange;
                }
                dialog.bsp-fixer::backdrop {
                    background-color: rgba(0,0,0,0.5);
                }
                button.bsp-fixer {
                    position: fixed;
                    right: 27px;
                    bottom: 80px;
                    box-sizing: content-box;
                    width: 36px;
                    height: 36px;
                    /*padding: 5px 10px;*/
                    border-radius: 10px;
                    z-index: 10000;
                    background-color: white;
                    border: 1px solid rgba(0,0,0,0.125);
                    color: rgb(82, 45, 114);
                    cursor: pointer;
                }
                button.bsp-fixer span.material-symbols-outlined {
                    margin-top: 5px;
                    margin-bottom: -5px;
                    outline: 0;
                }
                button.bsp-fixer:hover {
                    outline: 2px solid black;
                }
                button.bsp-fixer:active {
                    background-color: rgba(82, 45, 114, 0.5);
                    color: white;
                }
                dialog.bsp-fixer button {
                    margin: 0px 5px;
                    padding: 5px 10px;
                    border-radius: 5px;
                    cursor: pointer;
                    background-color: transparent;
                    border: 1px solid rgba(0,0,0,0.125);
                }
                dialog.bsp-fixer button:hover {
                    outline: 2px solid black;
                }
                dialog.bsp-fixer button:active {
                    background-color: rgba(82, 45, 114, 0.5);
                    color: white;
                }
            `
        })
    }


    ///////////////////////////////////////////////////////////////
    // FIXER FUNCTIONS

    // add google font icons https://fonts.google.com/icons
    addElement(document.head, 'link', {
        rel: 'stylesheet',
        //href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200',
        href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0',
    })
    // add basic styling
    addElement(document.head, 'style', {
        innerHTML: `
            a.d2l-link.preview-assignment {
                opacity: 50%;
                color: rgb(82, 45, 114);
                cursor: pointer;
            }
            a.d2l-link.preview-assignment:hover {
                opacity: 100%;
            }
            /* Remove extra space at the bottom of course-list waffle menu */
            .d2l-datalist-style1 > .d2l-datalist {
               padding-bottom: 0px;
            }        `,
    })

    function addNavBarLink(label, href, title) {
        let navbar = query('div.d2l-navigation-s-main-wrapper')
        let div = addElement(navbar, 'div', {
            className: 'd2l-navigation-s-item'
        })
        let a = addElement(div, 'a', {
            innerHTML: label,
            href: href,
            title: title || '',
            className: 'd2l-navigation-s-link bsp-fixer',
            style: 'color: rgb(82, 45, 114);',
        })
        return a
    }


    // full width content
    callWhenExists('div#root-wrapper', (rootwrapper) => {
        let leftpanel = rootwrapper.querySelector('div.navigation-container')
        let mainpanel = rootwrapper.querySelector('div.content-panel')
        rootwrapper.style.maxWidth = getOption('full width')
        leftpanel.style.width = getOption('panel width')
        mainpanel.style.width = "calc(" + getOption('full width') + " - " + getOption('panel width')
        mainpanel.style.left = getOption('panel width')
    })

    // populate course list
    // must wait just a bit for event listeners to get attached
    let callbackWhenClassList = []
    function callWhenClassListIsPopulated(fn) {
        callbackWhenClassList.push(fn)
    }
    callWhenExists('div.d2l-navigation-s-main-wrapper', (elem) => {
        let courses = query('div.d2l-navigation-s-course-menu d2l-navigation-dropdown-button-icon')
        setTimeout(() => {
            // once course list is populated, add courses to navbar
            callWhenExists('div.d2l-courseselector-wrapper', (courseselector) => {
                callbackWhenClassList.forEach((fn)=>{ fn(courseselector) })
                callbackWhenClassList = []
            })

            let ae = document.activeElement
            function toggleAndClean() {
                courses.removeEventListener('mouseup', toggleAndClean)
                ae.focus()
                setTimeout(() => {
                    courses.click()
                    setTimeout(()=>{
                        courses.blur()
                        ae.focus()
                        //if(courses.hasFocus()) {
                        //    document.body.focus()
                        //}
                    }, 100)
                    //triggerMouseEvent(courses, 'mouseup')
                    //triggerMouseEvent(courses, 'click')
                    callWhenExists('HEADER > NAV.d2l-navigation-s.d2l-branding-navigation-dark-foreground-color.d2l-navigation-s-linkarea-no-color > D2L-NAVIGATION > D2L-NAVIGATION-MAIN-HEADER > DIV.d2l-navigation-header-right > DIV.d2l-navigation-s-course-menu > d2l-navigation-dropdown-button-icon #shadow-root > d2l-tooltip', (tooltip) => {
                        // reshow course list
                        setTimeout(()=>{ courses.style.opacity = '' }, 200)
                        // hack to hide tooltip  (need all this??)
                        tooltip = tooltip.host
                        tooltip.style.visibility = 'hidden'
                        tooltip.removeAttribute('showing')
                        tooltip.ariaHidden = 'true'
                        let e = query('div.d2l-tooltip-content', tooltip.shadowRoot)
                        e.classList.remove('vdiff-target')
                        document.body.focus()
                    })
                }, 1000)
            }
            courses.style.opacity = '0%' // hide so course list doesn't flash
            courses.addEventListener('mouseup', toggleAndClean)
            triggerMouseEvent(courses, 'mouseup') // open courses to force populate
        }, 1000)
    })

    // tweak the course list
    callWhenClassListIsPopulated((courseselector) => {
        let pinned = []
        let unpinned = []
        courseselector.querySelectorAll('li').forEach((course) => {
            let icon = course.querySelector('d2l-button-icon')
            if(icon.classList.contains('d2l-hidden')) {
                unpinned.push(course)
            } else {
                pinned.push(course)
            }
        })
        courseselector.querySelectorAll('li').forEach((course) => {
            let hideUnpinned = (pinned.length > 0 && getOption('courselist hide unpinned') == 'true')
            if(pinned.indexOf(course) == -1) {
                // unpinned course
                if(hideUnpinned) {
                    course.style.display = 'none'
                    return
                }
            } else {
                // pinned course
                if(hideUnpinned) {
                    course.style.backgroundColor = 'white'
                }
            }
            if(getOption('courselist shorten') == 'true') {
                let link = course.querySelector('a.d2l-link')
                let name = link.innerHTML
                let semester = /Fall|Spring|Summer|Interterm/.exec(name)[0]
                let year = /20\d\d/.exec(name)[0]
                let coursenum = /([A-Z]{3})(-| )([0-9]{3}[A-Za-z0-9]*)/.exec(name)[0]
                let coursenum_ = coursenum.replaceAll(/-| /g,'')
                let title = name.replaceAll(semester, '').replaceAll(year, '').replaceAll(coursenum, '')
                title = title.replaceAll(/\(\d+\) - [0-9.]+/g, ' ').trim()
                title = title.replaceAll(/ +/g, ' ')
                //console.log([year,semester,coursenum,name,title])
                link.innerHTML = coursenum_ + ' ' + title
            }
        })
    })

    // move chat button in quick eval so it doesn't cover bsp fixer button
    setTimeout(()=>{
        addElement(document.head, 'style', {
            innerHTML: `
                iframe#chatFrame {
                    bottom: 120px !important;
                }
            `
        })
    }, 100)

    callWhenExists('header', (header) => { // div.d2l-body-main-wrapper header
        if(getOption('navbar sticky') == 'true') {
            //header.style.position = 'sticky'
            //header.style.top = '0'
            //header.style.zIndex = '100000'
            //header.style.background = 'rgba(255,255,255.0.9)'
            //header.style.backdropFilter = 'blur(5px)'
            addElement(document.head, 'style', {
                innerHTML: `
                    header {
                        position: sticky !important;
                        top: 0 !important;
                        z-index: 100000 !important;
                        background:linear-gradient(0deg, rgba(255,255,255,0.75), rgba(255,255,255,0.90), rgba(255,255,255,1.00)) !important;
                        backdrop-filter: blur(7px) !important;
                    }
                    header * {
                        background: transparent !important;
                    }
                    div.d2l-dialog.d2l-dialog-mvc {
                        top: 200px !important;
                    }
                    div.d2l-dialog-outer, div.d2l-dialog-fullscreen-mobile, div.d2l-dialog-outer-scroll {
                        top: 200px !important;
                        height: calc(100% - 300px);
                    }
                    div.ddial_o {
                        top: 200px !important;
                    }
                    div.d2l-dialog-outer, div.d2l-dialog-fullscreen-mobile, div.d2l-dialog-outer-scroll {
                        top: 200px !important;
                        height: calc(100% - 300px);
                    }
                    table tr[header]:nth-child(1) th {
                        top: calc(158px + 0px + var(--d2l-table-border-radius-sticky-offset, 0px)) !important;
                    }
                    table tr[header]:nth-child(2) th {
                        top: calc(158px + 66px + var(--d2l-table-border-radius-sticky-offset, 0px)) !important;
                    }
                `
            })
            setInterval(()=>{
                // DIV.d2l-body-main-wrapper > DIV.d2l-page-main.d2l-max-width.d2l-min-width > DIV#CourseImageBannerPlaceholderId.d2l-placeholder.d2l-placeholder-live > DIV.d2l-course-banner-container > D2L-IMAGE-BANNER-OVERLAY#d2l_1_71_530.d2l-image-banner-overlay #shadow-root > D2L-DIALOG-FULLSCREEN#basic-image-selector-overlay #shadow-root > DIV#d2l-uid-71.vdiff-target.d2l-dialog-outer.d2l-dialog-fullscreen-mobile.d2l-dialog-outer-overflow-bottom.d2l-dialog-outer-scroll
                function search(e) {
                    let children = null, i = 0
                    if(e.tagName == 'DIV' && e.classList.contains('d2l-dialog-outer')) {
                        if(!e.classList.contains('bspfixer-fixed')) {
                            // use classlist to prevent reprocessing
                            e.style.top = "200px"
                            e.style.height = "calc(100% - 300px)"
                            e.classList.add('bspfixer-fixed')
                        }
                    }
                    if(e.shadowRoot) {
                        children = e.shadowRoot.children
                        for(i = 0; i < children.length; i++) search(children[i])
                    }
                    children = e.children
                    for(i = 0; i < children.length; i++) search(children[i])
                }
                search(document.body)
            }, 500)
        }
    })

    // tweak the main navbar (add extra links, remove broken stuff)
    callWhenExists('div.d2l-navigation-s-main-wrapper', (elem) => {
        let courseTitle = query('div.d2l-navigation-s-title-container')
        if(courseTitle) {
            // course number is embedded in URL either as a URL parameter or part of URL
            // TODO: go directly to send email instead of the "confirm" page
            let params = new URLSearchParams(window.location.search)
            let course = params.get('ou')
            if(course == null) course = (/\d\d+/.exec(window.location.href))[0]

            // on course page
            if(getOption('navbar email') == 'true') {
                addNavBarLink(
                    'Email',
                    '/d2l/lms/classlist/print_email.d2l?pageOption=email&ou=' + course,
                    'Send an email to all the students',
                )
            }
            if(getOption('navbar grades') == 'true') {
                addNavBarLink(
                    'Grades',
                    '/d2l/lms/grades/index.d2l?ou=' + course,
                    'Gradebook',
                )
            }
            if(getOption('navbar announcement') == 'true') {
                addNavBarLink(
                    'Announce',
                    '/d2l/lms/news/newedit.d2l?ou=' + course + '&global=0',
                    'Create an announcement',
                )
            }
            if(getOption('navbar classlist') == 'true') {
                addNavBarLink(
                    'Classlist',
                    '/d2l/lms/classlist/classlist.d2l?ou=' + course,
                    'Display the classlist',
                )
            }
            if(getOption('navbar more') == 'false') {
                elem.querySelectorAll('div.d2l-navigation-s-more').forEach((d)=>{
                    setTimeout(()=>{
                        d.style.display = 'none'
                    }, 500)
                })
            }
        } else {
            // on main taylor page
            // REMOVE A FEW NAVBAR ITEMS BECAUSE THEY DO NOTHING!
            elem.querySelectorAll('div.d2l-navigation-s-item').forEach((navitem) => {
                let html = navitem.innerHTML
                let hide = false
                if(html.indexOf('>Content</a>') != -1) hide = true
                if(html.indexOf('Activities and Assessments') != -1) hide = true
                if(html.indexOf('Course Admin') != -1) hide = true
                if(html.indexOf('Progress') != -1) hide = true
                if(hide) {
                    // CANNOT SET DISPLAY: NONE, OTHERWISE ALL NAVBAR ITEMS DISAPPEAR!?
                    navitem.style = 'opacity: 0%; position: absolute; left: -10000px'
                }
            })

            // add links to pinned courses
            // must first populate course list
            // must wait just a bit for event listeners to get attached
            let courses = query('div.d2l-navigation-s-course-menu d2l-navigation-dropdown-button-icon')
            callWhenClassListIsPopulated((courseselector) => {
                courseselector.querySelectorAll('li').forEach((course) => {
                    let icon = course.querySelector('d2l-button-icon')
                    if(icon.classList.contains('d2l-hidden')) return
                    if(icon.icon.indexOf('pin-filled') == -1) return
                    let link = course.querySelector('a.d2l-link')
                    let name = link.innerHTML
                    let courseparts = /\([A-Z0-9, \-]+\)/.exec(name)
                    let coursenum = name.slice(0, name.indexOf(' '))
                    if(courseparts) {
                        coursenum = courseparts[0].slice(1, -1).replaceAll(', ','/')
                    }
                    addNavBarLink(
                        coursenum,
                        link.href,
                        name,
                    )
                })
            })
        }

        // always kill these items from the header, added 2024.05.15
        const remove = ['Anthology Course Evaluations']
        elem.querySelectorAll('div.d2l-navigation-s-item').forEach((d)=>{
            if(remove.indexOf(d.innerText.trim()) != -1) {
                d.style.display = 'none'
            }
        })

        if(getOption('navbar shorten') == 'true') {
            elem.querySelectorAll('span.d2l-navigation-s-group-text').forEach((span) => {
                if(span.innerHTML == 'Activities and Assessments') {
                    span.innerHTML = 'Assessments'
                }
            })
            elem.querySelectorAll('a').forEach((span) => {
                if(span.innerHTML == 'Quick Eval') {
                    span.innerHTML = 'Eval'
                    span.title = 'Quick Eval'
                }
                if(span.innerHTML == 'Course Admin') {
                    span.innerHTML = 'Admin'
                    span.title = 'Course Admin'
                }
                if(span.innerHTML == 'TU Helpdesk') {
                    span.innerHTML = 'Helpdesk'
                    span.title = 'TU Helpdesk'
                }
                if(span.innerHTML == 'TU Academic Catalogs') {
                    span.innerHTML = 'Catalogs'
                    span.title = 'TU Academic Catalogs'
                }
                if(span.innerHTML == 'Insights Portal') {
                    span.innerHTML = 'Insights'
                    span.title = 'Insights Portal'
                }
                // simplify date
                let t = span.innerHTML
                t = t.replace('Monday', 'Mon').replace('Tuesday', 'Tue').replace('Wednesday', 'Wed').replace('Thursday', 'Thu').replace('Friday', 'Fri')
                t = t.replace('Saturday', 'Sat').replace('Sunday', 'Sun')
                t = t.replace('January', 'Jan').replace('February', 'Feb').replace('March', 'Mar').replace('April', 'Apr').replace('May', 'May').replace('June', 'Jun')
                t = t.replace('July', 'Jul').replace('August', 'Aug').replace('September', 'Sep').replace('October', 'Oct').replace('November', 'Nov').replace('December', 'Dec')
                span.innerHTML = t
            })
        }
        if(getOption('date fixer') == 'true') {
            elem.querySelectorAll('a').forEach((span) => {
                let m = /((Mon|Tue|Wed|Thu|Fri|Sat|Sun)[a-z]*), ((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[a-z]*) (\d+), (\d+)/.exec(span.innerText)
                if(!m) return
                console.dir(m)
                span.innerText = m[1] + ' ' + m[6] + ' ' + m[3] + ' ' + m[5]
            })
        }
    })

    function expandAllModules() {
        let selected = document.querySelector('div.unit-box.selected')
        if(!selected) return
        document.querySelectorAll('div.unit-box').forEach((e) => {
            // important: clicking will change selected
            if(e == selected) {
                // make sure selected unit box is not expanded
                if(e.ariaExpanded == 'true') e.click()
            } else {
                if(e.ariaExpanded != 'true') e.click()
            }
        })
        // expand and re-select original selected unit box
        selected.click()
        // wait for everything to expand first, then scroll selected into view
        setTimeout(() => { selected.scrollIntoView() }, 1000)
    }
    // auto-expand the modules in Content view
    if(getOption('auto expand modules') == 'true') {
        callWhenExists('div.unit-box.selected', (e) => {
            expandAllModules()
        })
    }
    // add expand all button to panel navbar
    callWhenExists('div.nav-header > div.options-header', (nav) => {
        let divWrap = addElement(nav, 'div', {})
        let btnExpand = addElement(divWrap, 'button', {
            innerHTML: '+',
            title: 'Click to expand all of the modules',
        })
        btnExpand.addEventListener('click', expandAllModules)
    })

    // set pagination
    callWhenExists('select.d2l-select', (e) => {
        document.querySelectorAll('select.d2l-select').forEach((select) => {
            if(select.title.toLowerCase() != 'results per page') return
            for(let i = 0; i < select.options.length; i++) {
                if(i == select.selectedIndex) continue
                if(select.options[i].value != getOption('pagination')) continue

                select.selectedIndex = i
                // create an onchange event and dispatch it
                // note: doing so will force a reload by bsp
                var event = new Event('change')
                select.dispatchEvent(event)
            }
        })
    })

    // add links to preview assignment
    // note: found the preview URL under 3dots menu in Quick Eval
    callWhenExists('h1#d_page_title > span', (e) => {
        // add links on the All Assignments page
        if(e.innerHTML != 'Assignments') return
        var assigns = document.querySelectorAll('th.d_ich div.dco.d2l-foldername > div.dco_c')
        assigns.forEach((div) => {
            var eurl = div.querySelector('a').href
            //let params = new URLSearchParams(eurl)
            //let ou = params.get('ou')
            //let db = params.get('db')  <---- does not work??
            // console.log(eurl)
            var db = /db=(\d+)/.exec(eurl)[1]
            var ou = /ou=(\d+)/.exec(eurl)[1]
            var vurl = 'https://taylor.brightspace.com/d2l/lms/dropbox/user/folder_submit_files.d2l?ou=' + ou + '&db=' + db + '&grpid=0&isprv=1&bp=1'
            var view = addElement(div, 'a', {
                innerHTML: '<span class="material-symbols-outlined">search</span>',
                title: 'Click to preview this assignment in a new tab',
                href: vurl,
                target: '_blank',
                className: 'd2l-link d2l-link-inline preview-assignment',
                style: 'margin: 5px 8px -5px 0px',
            }, true)
        })
    })
    /*callWhenExists('div.jump-to-activity', (e) => {
        let btnGoto = query('.topic-jump-button', e)
        if(!btnGoto) return
        // "https://taylor.brightspace.com/d2l/le/lessons/11299/topics/73964"
        var aurl = document.referrer
        var ou = /le\/lessons\/(\d+)/.exec(aurl)[1]
        var db = /topics\/(\d+)/.exec(aurl)[1]      // <--- this is incorrect!
        var vurl = 'https://taylor.brightspace.com/d2l/lms/dropbox/user/folder_submit_files.d2l?ou=' + ou + '&db=' + db + '&grpid=0&isprv=1&bp=1'
        var view = addElement(e, 'a', {
            innerHTML: '<span class="material-symbols-outlined" style="transform:scale(1.5)">search</span>',
            title: 'Click to preview this assignment in a new tab',
            href: vurl,
            target: '_blank',
            className: 'd2l-link d2l-link-inline preview-assignment',
            style: 'margin-top: 20px',
        })
    })*/
    callWhenExists('h1#d_page_title', (h1) => {
        // add link to individual assignment page
        var aurl = window.location.href
        if(aurl.search(/lms\/dropbox\/admin\/mark\/folder_submissions_users/) == -1) return
        // https://taylor.brightspace.com/d2l/lms/dropbox/admin/mark/folder_submissions_users.d2l?ou=11299&db=20631&cfql=1
        var db = /db=(\d+)/.exec(aurl)[1]
        var ou = /ou=(\d+)/.exec(aurl)[1]
        var vurl = 'https://taylor.brightspace.com/d2l/lms/dropbox/user/folder_submit_files.d2l?ou=' + ou + '&db=' + db + '&grpid=0&isprv=1&bp=1'
        var view = addElement(h1, 'a', {
            innerHTML: '<span class="material-symbols-outlined" style="transform:scale(1.5)">search</span>',
            title: 'Click to preview this assignment in a new tab',
            href: vurl,
            target: '_blank',
            className: 'd2l-link d2l-link-inline preview-assignment',
            style: 'margin-right: 8px',
        }, true)
    })
    callWhenExists('D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > CONSISTENT-EVALUATION-RIGHT-PANEL #shadow-root > DIV.d2l-consistent-evaluation-right-panel > DIV.d2l-consistent-evaluation-right-panel-clearfix > D2L-DROPDOWN-MORE.d2l-consistent-evaluation-right-panel-overflow-menu > D2L-DROPDOWN-MENU > D2L-MENU > D2L-MENU-ITEM#d2l-uid-3 #shadow-root > DIV.d2l-menu-item-text', (preview) => {
        let title = query('D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > D2L-CONSISTENT-EVALUATION-NAV-BAR #shadow-root > DIV.d2l-consistent-evaluation-immersive-navigation > D2L-NAVIGATION-IMMERSIVE > DIV > DIV#titleName')
        // have to add link and style here? cannot see document?
        addElement(title, 'link', {
            rel: 'stylesheet',
            //href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200',
            href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0',
        })
        addElement(title, 'style', {
            innerHTML: `
            a.d2l-link.preview-assignment {
                opacity: 50%;
                color: rgb(82, 45, 114);
                cursor: pointer;
            }
            a.d2l-link.preview-assignment:hover {
                opacity: 100%;
            }
        `,
        })
        let btn = addElement(title, 'a', {
            innerHTML: '<span class="material-symbols-outlined" style="transform:translate(0px, 5px)">search</span>',
            title: 'Click to preview this assignment in a new tab',
            className: 'd2l-link d2l-link-inline preview-assignment',
        }, true)
        btn.addEventListener('click', ()=>{ preview.click() })
        title.addEventListener('click', ()=>{ preview.click() })
        title.style.cursor = 'pointer'
    })

    // pop out the instructions
    callWhenExists('form tr#z_j', (tr) => {
        let tbody = tr.parentNode
        tbody.querySelectorAll('td').forEach((td) => {
            let span = td.querySelector('span')
            if(!span || span.innerHTML != 'Instructions') return
            let ntd = td.parentNode.nextSibling.children[0]
            ntd.style.border = "1px solid rgb(82, 45, 114)"
            ntd.style.padding = "1rem"
            ntd.style.backgroundColor = "rgba(82, 45, 114, 0.125)"
            ntd.style.boxShadow = "0 0 10px rgba(82, 45, 114, 0.25)"
        })
    })

    // helper to create selector queries that drill through shadowDOMs
    let highlightElem = null
    let highlighter = null
    let highlighterOn = false
    function highlight(elem) {
        if(highlightElem) {
            highlightElem.style.outline = ''
        }
        highlightElem = elem
        clearInterval(highlighter)
        highlighter = setInterval(()=>{
            if(!highlightElem) return
            highlighterOn = !highlighterOn
            if(highlighterOn) {
                highlightElem.style.outline = '3px solid rgb(82, 45, 114)'
            } else {
                highlightElem.style.outline = '3px dashed rgb(82, 45, 114)'
            }
        }, 100)
    }
    let removeShadowReports = []
    function attachShadowDrill(e, tagnames) {
        function mm(e) {
            if(e.target.shadowRoot) { return } // ignore e.target element, because an element in shadowDOM will do the reporting
            highlight(e.target)
            console.log(createSelector(e.target))
        }
        e.addEventListener('mousemove', mm)
        removeShadowReports.push(()=>{ e.removeEventListener('mousemove', mm) })
        e.querySelectorAll('*').forEach((el) => {
            if(!el.shadowRoot) return
            let cur = el
            let tns = []
            while(cur.parentNode != e) {
                if(!cur.parentNode || cur == cur.parentNode) {
                    cur = cur.host
                } else {
                    cur = cur.parentNode
                }
                tns.push(cur.tagName)
            }
            let ntagnames = tagnames.concat(tns)
            ntagnames.push(el.tagName)
            // console.log('Drilling into: ' + ntagnames)
            attachShadowDrill(el.shadowRoot, ntagnames)
        })
    }
    function startSelectorReporting() {
        console.log('Reporting selector for hovered element for the next 2 seconds...')
        attachShadowDrill(document.body, ['body'])
        setTimeout(() => {
            console.log('DONE!')
            removeShadowReports.forEach((fn)=> { fn() })
            removeShadowReports = []
            highlight()
        }, 5000)
    }

    // auto click load more button!
    const selectorLoadMore = 'DIV.d2l-page-main.d2l-min-width.d2l-max-width > DIV.d2l-page-main-padding > D2L-QUICK-EVAL.d2l-token-receiver #shadow-root > D2L-QUICK-EVAL-SUBMISSIONS #shadow-root > D2L-QUICK-EVAL-SUBMISSIONS-TABLE #shadow-root > DIV.d2l-quick-eval-submissions-table-load-more-container > D2L-BUTTON.d2l-quick-eval-submissions-table-load-more #shadow-root > BUTTON.d2l-label-text'
    callWhenExists(selectorLoadMore, (btn) => {
        btn.click()
    })

    // full-width edit quiz
    const editQuizPane = 'd2l-activity-quiz-editor #shadow-root > d2l-activity-editor #shadow-root > d2l-template-primary-secondary #shadow-root > div.d2l-template-primary-secondary-content'
    callWhenExists(editQuizPane, (el) => {
        el.style.maxWidth = getOption('full width');
    })

    // full-width edit assignment
    const editAssignmentPane = 'd2l-activity-assignment-editor #shadow-root > d2l-activity-editor #shadow-root > d2l-template-primary-secondary #shadow-root > div.d2l-template-primary-secondary-content'
    callWhenExists(editAssignmentPane, (el) => {
        el.style.maxWidth = getOption('full width');
    })

    // Quick Eval: add a button that will save draft and next student
    //const selectorQEvalFeedback = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > CONSISTENT-EVALUATION-RIGHT-PANEL #shadow-root > DIV.d2l-consistent-evaluation-right-panel > DIV.d2l-consistent-evaluation-right-panel-evaluation > CONSISTENT-EVALUATION-RIGHT-PANEL-EVALUATION #shadow-root > DIV > D2L-CONSISTENT-EVALUATION-RIGHT-PANEL-BLOCK > DIV > D2L-CONSISTENT-EVALUATION-RIGHT-PANEL-FEEDBACK #shadow-root > D2L-HTMLEDITOR #shadow-root > DIV.d2l-htmleditor-label-flex-container > DIV.d2l-htmleditor-container.d2l-skeletize > DIV.d2l-htmleditor-flex-container'
    const selectorQEvalNextStudent = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > D2L-CONSISTENT-EVALUATION-FOOTER #shadow-root > DIV#footer-container'
    const selectorQEvalSave = 'DIV.d2l-button-container > D2L-BUTTON#consistent-evaluation-footer-save-draft #shadow-root > BUTTON.d2l-label-text'
    const selectorQEvalUpdate = 'DIV.d2l-button-container > D2L-BUTTON#consistent-evaluation-footer-update #shadow-root > BUTTON.d2l-label-text'
    const selectorQEvalNext = 'DIV.d2l-button-container > D2L-NAVIGATION-BUTTON-ICON#consistent-evaluation-footer-next-student #shadow-root > BUTTON'
    const selectorQEvalAlert = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > D2L-ALERT-TOAST #shadow-root > DIV.d2l-alert-toast-container.d2l-alert-toast-container-lowest.vdiff-target' // > D2L-ALERT #shadow-root > DIV.d2l-alert-text.d2l-alert-text-with-actions.d2l-body-standard'
    callWhenExists(selectorQEvalNextStudent, (footer) => {
        addElement(footer, 'style', {
            innerHTML: `
                div.bsp-fixer.qeval button {
                    cursor: pointer;
                    padding: 14px 24px;
                    border: 0px;
                    border-radius: 5px;
                    font-weight: bold;
                    font-family: 'Lato', 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
                    color: white;
                    background-color: rgba(82, 45, 114, 0.75);
                }
                div.bsp-fixer.qeval button:hover {
                    background-color: rgba(82, 45, 114, 1.00);
                }
            `
        })
        let divWrap = addElement(footer, 'div', { className: 'bsp-fixer qeval'})
        let btnSaveAndNext = addElement(divWrap, 'button', {
            innerHTML: 'Save and Next',
            title: 'Click to Save Draft and Go Next',
        })
        btnSaveAndNext.addEventListener('click', () => {

            let alerter = query(selectorQEvalAlert)
            // wait until alter shows itself
            let attempts = 20
            function clickNext() {
                // console.dir(alerter)
                // console.log(alerter.dataset.state)
                if(alerter.dataset.state != 'opening') {
                    attempts--
                    if(attempts > 0) setTimeout(clickNext, 100)
                    return
                }
                console.log('Quick Eval Nexting...')
                let btnNext = query(selectorQEvalNext, footer)
                if(btnNext) {
                    btnNext.click()
                }
            }
            clickNext()

            console.log('Quick Eval Saving...')
            let btnSave = query(selectorQEvalSave, footer)
            let btnUpdate = query(selectorQEvalUpdate, footer)
            if(btnSave) btnSave.click()
            else if(btnUpdate) btnUpdate.click()
        })
        // NOTE: the following doesn't work.  the editor is iframed :(
        /*callWhenExists(selectorQEvalFeedback, (editor) =>{
            console.log('Listening...')
            console.dir(editor)
            editor.addEventListener('keydown', (event) => {
                console.dir(event)
                if(!event.ctrlKey || event.code != 'ArrowRight') return
                btnSaveAndNext.click()
            }, {capture:true})
        })*/
    })

    // change background color when viewing as learner
    callWhenExists('div.d2l-navigation-s-personal-menu', (person) => {
        let html = person.innerHTML
        if(html.indexOf('Viewing as Learner') == -1 && html.indexOf('Viewing as Incomplete Student') == -1) return
        let bg = getOption('learner background')
        if(bg) document.body.style.backgroundColor = bg
    })

    // shorten title
    if(getOption('title shorten') == 'true') {
        callWhenExists('HEADER > NAV.d2l-navigation-s.d2l-branding-navigation-dark-foreground-color.d2l-navigation-s-linkarea-no-color > D2L-NAVIGATION > D2L-NAVIGATION-MAIN-HEADER > DIV.d2l-navigation-header-left > DIV.d2l-navigation-s-header-logo-area.d2l-navigation-s-header-no-home-icon > DIV.d2l-navigation-s-title-container > a', (a)=>{
            let name = a.innerHTML
            let semester = /Fall|Spring|Summer|Interterm/.exec(name)[0]
            let semester_ = semester == 'Summer' ? 'Su' : semester.slice(0,1)
            let year = /20\d\d/.exec(name)[0]
            let coursenum = /([A-Z]{3})(-| )([0-9]{3}[A-Za-z0-9]*)/.exec(name)[0]
            let coursenum_ = coursenum.replaceAll(/-| /g,'')
            let title = name.replaceAll(semester, '').replaceAll(year, '').replaceAll(coursenum, '')
            title = title.replaceAll(/\(\d+\)/g, '').replaceAll(/ +/g, ' ').trim()
            a.innerHTML = coursenum_ + ' ' + title + ' (' + semester_ + year.slice(2) + ')'
        })
        // 'DIV.d2l-body-main-wrapper > DIV.d2l-page-main.d2l-max-width.d2l-min-width > DIV#CourseImageBannerPlaceholderId.d2l-placeholder.d2l-placeholder-live > DIV.d2l-course-banner-container > D2L-IMAGE-BANNER-OVERLAY#d2l_1_71_9.d2l-image-banner-overlay #shadow-root > DIV#overlayContent.d2l-image-banner-overlay-content.d2l-visible-on-ancestor-target.d2l-image-banner-overlay-content-shown > DIV.d2l-image-banner-course-name-container > DIV.d2l-image-banner-course-name > H1#bannerTitle.menu-exists > SPAN'
        callWhenExists('D2L-IMAGE-BANNER-OVERLAY.d2l-image-banner-overlay #shadow-root > DIV#overlayContent.d2l-image-banner-overlay-content H1#bannerTitle.menu-exists > SPAN', (span)=>{
            let name = span.innerHTML
            let semester = /Fall|Spring|Summer|Interterm/.exec(name)[0]
            let semester_ = semester == 'Summer' ? 'Su' : semester.slice(0,1)
            let year = /20\d\d/.exec(name)[0]
            let coursenum = /([A-Z]{3})(-| )([0-9]{3}[A-Za-z0-9]*)/.exec(name)[0]
            let coursenum_ = coursenum.replaceAll(/-| /g,'')
            let title = name.replaceAll(semester, '').replaceAll(year, '').replaceAll(coursenum, '')
            title = title.replaceAll(/\(\d+\)/g, '').replaceAll(/ +/g, ' ').trim()
            span.innerHTML = coursenum_ + ' ' + title + ' (' + semester_ + year.slice(2) + ')'
        })
    }

    // add first and last buttons to QEval
    // can be used to preload all the submissions!
    const selectorQEContainer = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > D2L-CONSISTENT-EVALUATION-NAV-BAR #shadow-root > DIV.d2l-consistent-evaluation-immersive-navigation > D2L-NAVIGATION-IMMERSIVE > D2L-NAVIGATION-ITERATOR'
    const selectorQEButtons = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > D2L-CONSISTENT-EVALUATION-NAV-BAR #shadow-root > DIV.d2l-consistent-evaluation-immersive-navigation > D2L-NAVIGATION-IMMERSIVE > D2L-NAVIGATION-ITERATOR'
    const selectorQEPosition = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV > D2L-CONSISTENT-EVALUATION-NAV-BAR #shadow-root > DIV.d2l-consistent-evaluation-immersive-navigation > D2L-NAVIGATION-IMMERSIVE > D2L-NAVIGATION-ITERATOR > SPAN.d2l-iterator-text.d2l-label-text'
    const selectorQESubmission = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV.d2l-consistent-evaluation-page-primary-slot > D2L-CONSISTENT-EVALUATION-LEFT-PANEL #shadow-root > D2L-CONSISTENT-EVALUATION-EVIDENCE-ASSIGNMENT #shadow-root > D2L-CONSISTENT-EVALUATION-ASSIGNMENTS-SUBMISSIONS-PAGE #shadow-root > DIV.d2l-consistent-evaluation-submission-list-view > D2L-LIST > D2L-CONSISTENT-EVALUATION-ASSIGNMENTS-SUBMISSION-ITEM #shadow-root > D2L-LIST-ITEM > D2L-LIST-ITEM-CONTENT > H3.d2l-heading-3'
    const selectorQENoSubmission = 'D2L-CONSISTENT-EVALUATION.d2l-token-receiver #shadow-root > D2L-CONSISTENT-EVALUATION-PAGE #shadow-root > D2L-TEMPLATE-PRIMARY-SECONDARY#evaluation-template > DIV.d2l-consistent-evaluation-page-primary-slot > D2L-CONSISTENT-EVALUATION-LEFT-PANEL #shadow-root > D2L-CONSISTENT-EVALUATION-EVIDENCE-ASSIGNMENT #shadow-root > DIV.d2l-consistent-evaluation-no-submissions-padding > DIV.d2l-consistent-evaluation-no-submissions-container > DIV.d2l-consistent-evaluation-no-submissions.d2l-body-standard'
    callWhenExists(selectorQEContainer, (container)=> {
        container.host.style.gap = '0.8rem'
    })
    callWhenExists(selectorQEButtons, (buttoncontainer)=>{
        // have to add link and style here? cannot see document?
        addElement(buttoncontainer, 'link', {
            rel: 'stylesheet',
            //href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200',
            href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0',
        })
        addElement(buttoncontainer, 'style', {
            innerHTML: `
            span.bsp-button {
                opacity: 50%;
                color: rgb(82, 45, 114);
                cursor: pointer;
                border: 3px solid rgb(82, 45, 114);
                border-radius: 100px;
                transform: translate(0px, 2px);
                font-size: 20pt;
            }
            span.bsp-button:hover {
                opacity: 100%;
            }
        `,
        })

        const spanPos = query(selectorQEPosition)
        let urlFirst = null
        let urlLast = null
        function updateURLs() {
            let cur = spanPos.innerHTML
            let user = /User (\d+) of (\d+)/.exec(cur)
            if(user[1] == '1') urlFirst = window.location.href
            if(user[1] == user[2]) urlLast = window.location.href
        }
        updateURLs()
        function callWhenUserLoads(fn, pre) {
            pre = pre || spanPos.innerHTML
            if(spanPos.innerHTML == pre) {
                setTimeout(()=>{ callWhenUserLoads(fn, pre) }, 100)
            } else {
                fn()
            }
        }
        let btnPrev = null
        let btnNext = null
        buttoncontainer.querySelectorAll('d2l-navigation-button-icon').forEach((button)=>{
            if(button.text == 'Previous') btnPrev = button
            if(button.text == 'Next') btnNext = button
        })
        btnPrev.addEventListener('click', (e)=>{ callWhenUserLoads(updateURLs) })
        btnNext.addEventListener('click', (e)=>{ callWhenUserLoads(updateURLs) })
        function clicker(pre, btn, ticks) {
            let cur = spanPos.innerHTML
            if(btn.disabled) return // reached end!
            if(cur == pre) {
                // still on same user, so wait just a bit more...
                setTimeout(()=>{ clicker(pre, btn, ticks+1) }, 100)
                return
            }
            // user submission vs no submission has different page layout, so we need to
            // watch for both cases.  but, we only want to click once per user.  done keeps
            // track of whether a click has been done.
            let done = false
            function doclick(e) {
                if(done) return
                done = true
                clicker(cur, btn, 0)
                // wait to click button based on how long it took to load in user submission
                // longer load time --> longer delay until click
                setTimeout(()=>{ btn.click() }, (ticks>2 ? 500 : 100))
            }
            callWhenExists(selectorQESubmission, doclick)
            callWhenExists(selectorQENoSubmission, doclick)
        }
        let btnRewind = addElement(buttoncontainer, 'span', {
            innerHTML: '<span class="material-symbols-outlined bsp-button">fast_rewind</span>',
            title: 'Click to advance to first',
        }, true)
        let btnFirst = addElement(buttoncontainer, 'span', {
            innerHTML: '<span class="material-symbols-outlined bsp-button">first_page</span>',
            title: 'Click to either jump to first (if seen) or advance to first',
            style: 'display: none', // do not show, because it forces a reload
        }, true)
        let btnForward = addElement(buttoncontainer, 'span', {
            innerHTML: '<span class="material-symbols-outlined bsp-button">fast_forward</span>',
            title: 'Click to advance to last',
        }, false)
        let btnLast = addElement(buttoncontainer, 'span', {
            innerHTML: '<span class="material-symbols-outlined bsp-button">last_page</span>',
            title: 'Click to either jump to last (if seen) or advance to last',
            style: 'display: none', // do not show, because it forces a reload
        }, false)
        btnRewind.addEventListener('click', ()=>{ clicker('', btnPrev, 0) })
        btnFirst.addEventListener('click', ()=>{
            clicker('', btnPrev, 0)
        })
        btnForward.addEventListener('click', ()=>{ clicker('', btnNext, 0) })
        btnLast.addEventListener('click', ()=>{ clicker('', btnNext, 0) })
    })

    // indicate better when new lesson is not in grade book
    const selectorIsHiddenLesson = 'D2L-ACTIVITY-CONTENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR #shadow-root > DIV#editor-container > D2L-TEMPLATE-PRIMARY-SECONDARY > D2L-ACTIVITY-EDITOR-FOOTER.d2l-activity-editor-footer #shadow-root > DIV.d2l-activity-editor-footer-left.d2l-skeletize > D2L-ACTIVITY-VISIBILITY-EDITOR #shadow-root > DIV.content-visibility > D2L-ACTIVITY-VISIBILITY-EDITOR-TOGGLE #shadow-root > DIV > D2L-SWITCH-VISIBILITY #shadow-root > SPAN.d2l-switch-text > SPAN'
    const selectorPrimaryLesson = 'D2L-ACTIVITY-CONTENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR #shadow-root > DIV#editor-container > D2L-TEMPLATE-PRIMARY-SECONDARY > DIV.d2l-primary-panel'
    if(window.location.href.match(/\/d2l\/le\/lessons\/[0-9]+\/edit\//)) {
        let hidden = null
        let primary = null
        setInterval(() => {
            let badHidden = false
            if(!hidden) hidden = query(selectorIsHiddenLesson)
            if(!primary) primary = query(selectorPrimaryLesson)
            if(hidden && primary) {
                let isVisible = (hidden.innerHTML.indexOf('Hidden') == -1)
                badHidden = !isVisible
                hidden.parentNode.parentNode.host.style.backgroundColor = (isVisible ? '' : 'rgba(27, 54, 95, 0.75)')
                hidden.parentNode.parentNode.host.style.color = (isVisible ? '' : 'white')
                hidden.parentNode.parentNode.host.style.paddingRight = '0.5em'
                hidden.parentNode.parentNode.host.style.borderRadius = '0.5em'
            }
            if(primary) {
                if(badHidden) {
                    let top = 'white' // badGradebook ? 'rgba(222, 79, 61, 0.5)' : 'white'
                    let bot = badHidden ? 'rgba(27, 54, 95, 0.5)' : 'white'
                    primary.style.backgroundImage = 'linear-gradient(' + top + ', ' + bot + ')'
                } else {
                    primary.style.backgroundImage = ''
                }
            }
        }, 500)
    }

    // indicate better when an assignment is not in grade book
    const selectorInGradeBook = 'D2L-ACTIVITY-ASSIGNMENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR > DIV.d2l-activity-assignment-editor-primary-panel > D2L-ACTIVITY-ASSIGNMENT-EDITOR-DETAIL #shadow-root > DIV#score-and-due-date-container > DIV#score-container > D2L-ACTIVITY-SCORE-EDITOR #shadow-root > DIV#score-info-container > DIV#grade-info-container > D2L-DROPDOWN > BUTTON.d2l-label-text.d2l-grade-info.d2l-dropdown-opener > DIV'
    const selectorIsHidden = 'D2L-ACTIVITY-ASSIGNMENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR #shadow-root > DIV#editor-container > D2L-TEMPLATE-PRIMARY-SECONDARY > D2L-ACTIVITY-EDITOR-FOOTER.d2l-activity-editor-footer #shadow-root > DIV.d2l-activity-editor-footer-left.d2l-skeletize > D2L-ACTIVITY-VISIBILITY-EDITOR #shadow-root > D2L-ACTIVITY-VISIBILITY-EDITOR-TOGGLE #shadow-root > D2L-SWITCH-VISIBILITY #shadow-root > SPAN.d2l-switch-text > SPAN'
    const selectorPrimary = 'D2L-ACTIVITY-ASSIGNMENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR #shadow-root > DIV#editor-container > D2L-TEMPLATE-PRIMARY-SECONDARY > DIV.d2l-primary-panel'
    if(window.location.href.indexOf('/d2l/le/activities/edit') != -1) {
        let gradebook = null
        let hidden = null
        let primary = null
        setInterval(()=> {
            let badGradebook = false
            let badHidden = false
            if(!gradebook) gradebook = query(selectorInGradeBook)
            if(gradebook) {
                // ungraded does not give option for grade book
                let inGradeBook = (gradebook.innerHTML.indexOf('Not in Grade Book') == -1)
                gradebook.parentNode.style.backgroundColor = (inGradeBook ? '' : 'rgba(222, 79, 61, 0.5')
                badGradebook = !inGradeBook
            }
            if(!hidden) hidden = query(selectorIsHidden)
            if(!primary) primary = query(selectorPrimary)
            if(hidden && primary) {
                let isVisible = (hidden.innerHTML.indexOf('Hidden') == -1)
                badHidden = !isVisible
                hidden.parentNode.parentNode.host.style.backgroundColor = (isVisible ? '' : 'rgba(27, 54, 95, 0.75)')
                hidden.parentNode.parentNode.host.style.color = (isVisible ? '' : 'white')
                hidden.parentNode.parentNode.host.style.paddingRight = '0.5em'
                hidden.parentNode.parentNode.host.style.borderRadius = '0.5em'
            }
            if(primary) {
                if(badGradebook || badHidden) {
                    let top = badGradebook ? 'rgba(222, 79, 61, 0.5)' : 'white'
                    let bot = badHidden ? 'rgba(27, 54, 95, 0.5)' : 'white'
                    primary.style.backgroundImage = 'linear-gradient(' + top + ', ' + bot + ')'
                } else {
                    primary.style.backgroundImage = ''
                }
            }
        }, 500)
    }
    // indicate better assignments not in grade book or hidden from all Assignments page
    callWhenExists('h1#d_page_title > span', (e) => {
        if(e.innerHTML != 'Assignments') return
        var assigns = document.querySelectorAll('th.d_ich div.dco.d2l-foldername > div.dco_c')
        assigns.forEach((div) => {
            let inGradeBook = false
            let isHidden = false
            div.querySelectorAll('span.di_s > img.di_i').forEach((img)=>{
                if(img.title.startsWith('Grade item:')) inGradeBook = true
                if(img.title.startsWith('Hidden')) isHidden = true
            })
            if(!inGradeBook) {
                div.parentNode.parentNode.style.backgroundColor = 'rgba(222, 79, 61, 0.1)'
                div.innerHTML += '<span style="font-weight:bold">NOT IN GRADEBOOK</span>'
            }
            if(isHidden) {
                div.parentNode.parentNode.style.backgroundColor = 'rgba(27, 54, 95, 0.1)'
                div.innerHTML += '<span style="font-weight:bold">NOT VISIBLE TO STUDENTS</span>'
            }
        })
    })

    // indicate better hidden units
    const selectorIsUnitHidden = 'D2L-ACTIVITY-CONTENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR #shadow-root > DIV#editor-container > D2L-TEMPLATE-PRIMARY-SECONDARY > D2L-ACTIVITY-EDITOR-FOOTER.d2l-activity-editor-footer #shadow-root > DIV.d2l-activity-editor-footer-left.d2l-skeletize > D2L-ACTIVITY-VISIBILITY-EDITOR #shadow-root > D2L-ACTIVITY-VISIBILITY-EDITOR-TOGGLE #shadow-root > D2L-SWITCH-VISIBILITY #shadow-root > SPAN#d2l-uid-53.d2l-switch-text > SPAN'
    const selectorUnitPrimary = 'D2L-ACTIVITY-CONTENT-EDITOR.d2l-token-receiver #shadow-root > D2L-ACTIVITY-EDITOR #shadow-root > DIV#editor-container > D2L-TEMPLATE-PRIMARY-SECONDARY > DIV.d2l-primary-panel'
    if(window.location.href.indexOf('/d2l/le/lessons/') != -1) {
        let hidden = null
        let primary = null
        setInterval(()=> {
            if(!hidden) hidden = query(selectorIsUnitHidden)
            if(!primary) primary = query(selectorUnitPrimary)
            if(hidden && primary) {
                let isVisible = (hidden.innerHTML.indexOf('Hidden') == -1)
                primary.style.backgroundImage = (isVisible ? '' : 'linear-gradient(white, rgba(27, 54, 95, 0.5))')
                hidden.parentNode.parentNode.host.style.backgroundColor = (isVisible ? '' : 'rgba(27, 54, 95, 0.75)')
                hidden.parentNode.parentNode.host.style.color = (isVisible ? '' : 'white')
                hidden.parentNode.parentNode.host.style.paddingRight = '0.5em'
                hidden.parentNode.parentNode.host.style.borderRadius = '0.5em'
            }
        }, 100)
    }


    // highlight table cells for gradebook when there are new submissions or unpublished feedback
    const selectorGrade_Row = 'tr:has(img.di_i)' // img.di_i indicates item has entry in gradebook
    setTimeout(()=>{
        document.querySelectorAll(selectorGrade_Row).forEach((tr)=>{
            const newSub = tr.querySelector('td:nth-child(3)')
            const evaled = tr.querySelector('td:nth-child(5)')
            const published = tr.querySelector('td:nth-child(6)')
            if(!newSub || !evaled || !published) return
            if(newSub.innerText.trim()) {
                newSub.style.backgroundColor = 'rgba(255,0,0,0.125)'
            }
            if(published.innerText.trim() != evaled.innerText.trim()) {
                published.style.backgroundColor = 'rgba(255,0,0,0.125)'
            }
        })
    }, 500)

    // kill vertical scrollbar on class progress view
    const selectorProgressView = 'DIV.d2l-page-main.d2l-min-width.d2l-max-width > DIV.d2l-page-main-padding > DIV.d2l-placeholder.d2l-placeholder-live > DIV.d2l-grid-wrapper-full > D2L-TABLE-WRAPPER #shadow-root > D2L-SCROLL-WRAPPER #shadow-root > DIV.d2l-scroll-wrapper-container.d2l-scroll-wrapper-focus'
    callWhenExists(selectorProgressView, (div)=>{
        console.dir(div)
        div.style.overflowX = 'visible'
    })

    // make set grade dialog bigger!
    setTimeout(()=>{
        let s = document.createElement('style')
        let html = ''
        html += 'div.ddial_o { height: 18em !important; }'
        html += 'div.ddial_i { height: 100% !important; }'
        html += 'div.ddial_c { height: auto !important; }'
        s.innerHTML = html
        document.body.appendChild(s)
    }, 100)

    // workaround fix for invisible navbar, added 2024.05.19
    setTimeout(()=>{
        let e = document.querySelector('div.d2l-navigation-s-main-wrapper[data-loading]')
        if(!e) return
        e.removeAttribute('data-loading')
    }, 500)


    // improve page title
    setTimeout(()=>{
        let m = document.title.match(/((.*) - )?(Spring|Fall|Summer|Interterm) (\d+) ([A-Z]+) ([0-9]+[a-zA-Z]?) (.*?) \((\d+)\)/)
        if(!m) return
        let page = m[2]
        let cprefix = m[5]
        let cnumber = m[6]
        let cdescription = m[7]
        let crn = m[8]
        let semester = m[3]
        let year = m[4]
        let t = ''
        if(page) {
            t = cprefix + cnumber + ' ' + page + ' - ' + cdescription + ' (' + semester.slice(0,2) + year.slice(2) + ', ' + crn + ')'
        } else {
            t = cprefix + cnumber + ' ' + cdescription + ' (' + semester.slice(0,2) + year.slice(2) + ', ' + crn + ')'
        }
        console.log(document.title)
        console.log(t)
        document.title = t
    }, 1000)
})();








