# Brightspace Fixer

This repo contains a [Tampermonkey](https://www.tampermonkey.net/) script to improve UX for [Brightspace](https://taylor.brightspace.com).


## Getting Started

- Install the [Tampermonkey](https://www.tampermonkey.net/) extension into your favorite browser (supports Chrome, Edge, Firefox, Safari, Opera Next)
- Click [link](https://repo.cse.taylor.edu/csedept/brightspace-fixer/-/raw/main/tampermonkey.user.js?ref_type=heads)
- When Tampermonkey shows up with the script, click Install.


## Updating

There are two ways to update to the latest version.

Option 1

- Click [link](https://repo.cse.taylor.edu/csedept/brightspace-fixer/-/raw/main/tampermonkey.user.js?ref_type=heads)
- When Tampermonkey shows up with the script, click Reinstall.

Option 2

- Open the Tampermonkey extension in your browser
- Open the Dashboard
- Click on the Brightspace Fixer
- Click the Settings tab (just above the File, Edit, ... menu)
- Click `Check for userscript updates`
- Follow any additional steps


## Contributing

Clone the repo, make the changes, commit and push!

